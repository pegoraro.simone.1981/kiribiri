-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: sql112.epizy.com
-- Creato il: Apr 25, 2021 alle 16:18
-- Versione del server: 5.6.48-88.0
-- Versione PHP: 7.2.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `epiz_27432170_kiribiri`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `cryptokey`
--

CREATE TABLE `cryptokey` (
  `cryptokey` varchar(16) NOT NULL,
  `likelihood` int(11) NOT NULL,
  `timeofbirth` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `clientinstance` int(11) NOT NULL,
  `likelihood_version` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `cryptokey`
--
ALTER TABLE `cryptokey`
  ADD PRIMARY KEY (`cryptokey`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
