function Breed(dad, mum){
    var dad_partial_genoma = dad.substr(0, dad.length/2);
    var mum_partial_genoma = mum.substr(mum.length/2, mum.length/2);

    var child_genoma = dad_partial_genoma + mum_partial_genoma;

    return Mutate(child_genoma);
}

function RandomChar(){
    rand_int = Math.floor(Math.random()*alphabet_num.length) + 1;
    return alphabet_num[Object.keys(alphabet_num).find(key => alphabet_num[key].n === rand_int)].c;
}

function _Brutate(genoma){
    return genoma.substr(0, (genoma.length/2)-1) + "mutatingchaos" + RandomChar() + RandomChar();
}

function _MutateGenes(genoma, n_of_genes_to_mutate){
    genoma_array = [...genoma];
    for(ix = 0; ix < n_of_genes_to_mutate; ix++)
        genoma_array[Math.floor(Math.random() * genoma_array.length)] = RandomChar();
    return genoma_array.join("");
}

function _AddGenes(genoma, n_of_genes_to_mutate){
    var genes = "";
    for(ix = 0; ix < n_of_genes_to_mutate; ix++){
        genes = genes + RandomChar();
    }

    return genoma + genes;
}

function _MutateRandSubstitution(genoma){
    var char_mutating = RandomChar();
    var char_mutation = RandomChar();
    
    return genoma.replace(char_mutating, char_mutation);
}

function Mutate(genoma){
    if(Math.random() < 0.054321)
        genoma = _Brutate(genoma);

    if(Math.random() < 0.04321)
        genoma = _MutateGenes(genoma, Math.floor(Math.random()*3)+1);

    if(Math.random() < 0.0123456789)
        genoma = _AddGenes(genoma, Math.floor(Math.random()*3)+1);

    if(Math.random() < 0.0987654321)
        genoma = _MutateRandSubstitution(genoma);
	
	return genoma;
}

function GenerateOffspring(parents){
    offspring = [].concat(...Object.keys(parents).map((dad, dad_index) => { 
        return Object.keys(parents).map((mum, mum_index) => {
            if(mum_index==dad_index) 
                return undefined; //can't breed with itself... so this entry will be removed by the filter below
            return Breed(dad, mum);
        });
    }).map(children => children.filter(child => child !== undefined)));
	
	return [...new Set(offspring)];
}

