importScripts("italian.js", "alphabet.js", "kiribiri.js", "ga.js");

let CLIENT_INSTANCE_ID = 0;

let cryptokeys = [];

const ERROR_OVER_ALPHABET = '*';
const ERROR_UNDER_ALPHABET = '-';

const URL_CRYPTOKEY_REST = '../crypto-key.php';


///------------------------------------------
/// Conversion char <--> num

function num2txt(nn){
    for(index = 0; index < alphabet_num.length; index++){
        if(alphabet_num[index].n == nn)
            return alphabet_num[index].c;
    }

    return nn > 0 ? ERROR_OVER_ALPHABET : ERROR_UNDER_ALPHABET;
}

function txt2num(key_ch){
    for(index = 0; index < alphabet_num.length; index++){
        if(alphabet_num[index].c == key_ch)
            return alphabet_num[index].n;
    }

    return 0;
}


///------------------------------------------
/// Server API:
/// Load existing crypto-keys

function GetCryptoKeyBreeders(){
    return new Promise(function(resolve, reject){
        const CRYPTOKEY_BREEDERS_URL = URL_CRYPTOKEY_REST + '?q=stochastic';

        fetch(CRYPTOKEY_BREEDERS_URL)
            .then(response => response.json())
            .then(json => {
                resolve(json);
            })
            .catch(err => {
                console.log('Breeders request failed', err);
                setTimeout(function(){GetCryptoKeyBreeders(retry-1);}, 5000);
            });
    });
}


/// Save new crypto-key
let cryptokeybuffer = [];
let cryptokeybuffer_index = 0;

let cryptokeybuffer_size = 1;  //start with low buffering then increase buffer size (so the client that will exit after few seconds don't lose result)

function IncreaseBufferSize(){  //dinamically increase size up to a limit
    const CRYPTOKEYBUFFER_MAX_LENGTH = 4096;

    if(cryptokeybuffer_size < CRYPTOKEYBUFFER_MAX_LENGTH)
        cryptokeybuffer_size *= 2;
}

function SaveCryptoKey(cryptokey, likelihood){
    cryptokeybuffer[cryptokeybuffer_index] = {cryptokey, likelihood, CLIENT_INSTANCE_ID};
    cryptokeybuffer_index++;
    if(cryptokeybuffer_index >= cryptokeybuffer_size){
        console.log("SAVING " + cryptokeybuffer_index + " results...");
        cryptokeybuffer_index = 0;
        IncreaseBufferSize();

        fetch(URL_CRYPTOKEY_REST, {
            method: "POST",
            body: JSON.stringify(cryptokeybuffer),
            headers: {"Content-type": "application/json; charset=UTF-8"}
        })
        .then(response => response.json())
        .then(json => {
            //console.log("POST (" + cryptokey + "):" + json);
        })
        .catch(err => {
            console.log('Save result failed', err)
        });
    }
}



function GetLastInitWord(){
    return new Promise(function(resolve, reject){
        const CRYPTOKEY_BREEDERS_URL = URL_CRYPTOKEY_REST + '?q=last_dict';

        fetch(CRYPTOKEY_BREEDERS_URL)
            .then(response => response.json())
            .then(json => {
                resolve(Object.keys(json)[0]);
            })
            .catch(err => {
                console.log('Breeders request failed', err)
                reject(err);
            });
    });
}


///------------------------------------------
/// Decypher, compute likelihood, save result

let chunk_cache = {};

function calculateChunkLikelihood(chunk, dictionary){

    if(chunk_cache[chunk] == undefined){
        chunk_cache[chunk] = dictionary.reduce(function(tot_occurences, words){
            return tot_occurences + words.reduce(function (occurences, word){
                if(word.includes(chunk))
                    occurences++;

                return occurences;
            }, 0);
        }, 0);
    }
    
    return chunk_cache[chunk];
}

function calculateLikelihood(txt, dictionary){
    const MAX_SUBSTR_LENGTH = 3;
    let chunks = [];
    for(index = 0; index < txt.length - MAX_SUBSTR_LENGTH + 1; index++){
        chunks[index] = txt.substring(index, MAX_SUBSTR_LENGTH+index);
    }

    likelihood_result = chunks.map(function (chunk){
        if(chunk.includes(ERROR_UNDER_ALPHABET) || chunk.includes(ERROR_OVER_ALPHABET))
            return 0;

        return calculateChunkLikelihood(chunk, dictionary);
    }).reduce(function(tot, curr){
        tot = tot+curr; 
        return tot;
    }, 0);

    return likelihood_result;
}

function DecypherAndSave(key_txt, dictionary){
	let key_char_array = key_txt.split('');
	let key = key_char_array.map(txt2num);

	linear_decyphered_num_sub = linear_cyph_text.map(function decyph(elem, i){ return elem - key[i % key.length] + 1; });
	linear_decyphered_txt_sub = linear_decyphered_num_sub.map(num2txt);

	postMessage(linear_decyphered_txt_sub.join(" "));

	linear_decyphered_txt = linear_decyphered_txt_sub.join("");
	likelihood = calculateLikelihood(linear_decyphered_txt, dictionary);
    
    SaveCryptoKey(key_txt, likelihood);

	return {key_txt, likelihood};
}



///------------------------------------------
/// New generations of crypto-keys...

function InitPopulation(dict, word_to_skip, fDecypher){
    console.log("Skip: " + word_to_skip);
    if(word_to_skip.length){
        var already_skipped = false;
        dict.forEach(letter => {
            letter.forEach(word => {
                if(already_skipped == false){
                    if(word.localeCompare(word_to_skip) <= 0)
                        return; //skip words...

                    console.log();
                    already_skipped = true; //end condition
                }
                
                fDecypher(word, dict);
            });
        });
    }
    else
	    dict.forEach(letter => letter.forEach(word => fDecypher(word, dict)));
}

function MakeNewGeneration(parent_cryptokeys, dictionary){
	cryptokeys = GenerateOffspring(parent_cryptokeys).map(function(ckey) {
		DecypherAndSave(ckey, dictionary);
	});
}



function StartSeekingSolutions(dictionary){
    GetCryptoKeyBreeders()
    .then(parent_cryptokeys => {
		if(Object.keys(parent_cryptokeys).length)
			MakeNewGeneration(parent_cryptokeys, dictionary);
		else
			InitPopulation(dictionary, '', DecypherAndSave);
        
        setTimeout(function(){
            StartSeekingSolutions(italian_dictionary);
        }, 1000);
    })
    .catch(err => {console.log("ERROR: " + err)});
}


//worker function: start
onmessage = function(e) {
    CLIENT_INSTANCE_ID = e.data;
    
    if(CLIENT_INSTANCE_ID)
        StartSeekingSolutions(italian_dictionary);  
    else{   //invoked by init script
        GetLastInitWord()
        .then(lastword => InitPopulation(italian_dictionary, lastword, DecypherAndSave));
    }
}
