<?php
require_once("env.php");

function ReadRecords($db, $sql){
    if($result = $db->query($sql)){
        $crypto_keys_records = array();
        while($row = $result->fetch_assoc())
            $crypto_keys_records[] = $row;

        $result->free();
        return $crypto_keys_records;
    }

    return [];
}

function ReadTopRecord($db, $tableName, $columnName){
    $sql="SELECT * FROM $tableName order by $columnName DESC LIMIT 1";
    
    return ReadRecords($db, $sql)[0];
}

function CountRecords($db, $tableName){
    $result = $db->query("SELECT COUNT(*) FROM $tableName");
    $row = $result->fetch_row();
    return $row[0];
}

function ReadNewestRecord($db, $tableName, $columnTimestamp, $limitMinute){
    $sql = "SELECT * FROM $tableName WHERE $columnTimestamp >= NOW() - INTERVAL $limitMinute MINUTE";

    return ReadRecords($db, $sql);
}

//////////////////////////

$db = new mysqli($DB_HOST, $DB_USER, $DB_PASSWORD, $DB_NAME);
$tableName = "cryptokey";

if($db->connect_errno)
    die("Error " . $db->connect_error);

$stats = array();

$stats['best'] = ReadTopRecord($db, $tableName, "likelihood");
$stats['last_created'] = ReadTopRecord($db, $tableName, "timeofbirth");
$stats['last5minute'] = count(ReadNewestRecord($db, $tableName, "timeofbirth", 5));
$stats['last30minute'] = count(ReadNewestRecord($db, $tableName, "timeofbirth", 30));
$stats['last1hour'] = count(ReadNewestRecord($db, $tableName, "timeofbirth", 60));
$stats['last5hour'] = count(ReadNewestRecord($db, $tableName, "timeofbirth", 300));
$stats['count'] = CountRecords($db, $tableName);

echo json_encode($stats);

?>