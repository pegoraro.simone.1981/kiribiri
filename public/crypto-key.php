<?php
require_once("env.php");


$db = new mysqli($DB_HOST,   $DB_USER, $DB_PASSWORD, $DB_NAME);

if($db->connect_errno)
    die("Error " . $db->connect_error);

$method = $_SERVER['REQUEST_METHOD'];

if($method == "GET"){
    if($_REQUEST["q"] == 'stochastic'){
        $cntRange1 = "30";
        $thr1 = "60/100";
        $cntRange2 = "15";
        $thr2 = "40/100";
        $cntRange3 = "9";
        $thr3 = "20/100";
        $cntRange4 = "6";

        $sql = "(SELECT * FROM cryptokey, (select max(likelihood) as maxl from cryptokey) as maxt WHERE likelihood > maxt.maxl*$thr1 ORDER BY RAND() LIMIT $cntRange1) ".
        "UNION  (SELECT * FROM cryptokey, (select max(likelihood) as maxl from cryptokey) as maxt  WHERE likelihood <= maxt.maxl*$thr1 AND likelihood > maxt.maxl*$thr2 ORDER BY RAND() LIMIT $cntRange2) ".
        "UNION  (SELECT * FROM cryptokey, (select max(likelihood) as maxl from cryptokey) as maxt  WHERE likelihood <= maxt.maxl*$thr2 AND likelihood > maxt.maxl*$thr3 ORDER BY RAND() LIMIT $cntRange3) ".
        "UNION  (SELECT * FROM cryptokey, (select max(likelihood) as maxl from cryptokey) as maxt  WHERE likelihood <= maxt.maxl*$thr3 ORDER BY RAND() LIMIT $cntRange4) ";
    }
    elseif($_REQUEST["q"] == 'last_dict')
        $sql="SELECT * FROM cryptokey WHERE clientinstance = 0 order by cryptokey DESC LIMIT 1";
    elseif(isset($_REQUEST["best"])){
        $max_limit = 10000;
        $limit = $_REQUEST["best"];
        if(is_numeric($limit)){
            if($limit > $max_limit)
                $limit = $max_limit;
        }
        else
            $limit = $max_limit;

        $sql="SELECT cryptokey, likelihood FROM cryptokey order by likelihood DESC LIMIT $limit";
    }
    else
        $sql="SELECT * FROM cryptokey order by likelihood DESC LIMIT 100";

    if($result = $db->query($sql)){
        $crypto_keys_records = array();
        while($row = $result->fetch_assoc())
            $crypto_keys_records[] = $row;

        $result->free();
        $crypto_keys = array_column($crypto_keys_records, 'likelihood', 'cryptokey');

        echo json_encode($crypto_keys);
    }
}
elseif($method == "POST"){
    $data_array = json_decode(file_get_contents('php://input'), true);
    $sql = "insert ignore into cryptokey (cryptokey, likelihood, clientinstance, likelihood_version) values ";
    foreach($data_array as $data){
        if(isset($data["cryptokey"]) && isset($data["likelihood"]) && isset($data["CLIENT_INSTANCE_ID"]) ){
            $cryptokey = "'".$data["cryptokey"]."'";
            $likelihood = $data["likelihood"];
            $clientinstance = $data["CLIENT_INSTANCE_ID"];
            $likelihood_version = $data["likelihood_version"] ?? 0;

            $sql .= "($cryptokey, $likelihood, $clientinstance, $likelihood_version), ";
        }
    }
    if($db->query(rtrim($sql, ", ")) === TRUE)
        $result = "OK"; 
    else
        $result = "ERROR: $sql - " .$db->error;

    echo json_encode($result);
}

$db->close();

?>