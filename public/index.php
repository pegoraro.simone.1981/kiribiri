<html>
    <head>
<style>
#maincontainer {
    width:100%;
    height: 100%;
}

#decyphered_txt_sub {
    width: 210px;
    background: #eeeeee;
    margin: auto;
}
</style>

<script type="text/javascript">

<?php
    echo "const _CLIENT_INSTANCE_ID = ".mt_rand().";";
?>

var worker = new Worker("js/crypto.js");

function w_start(){
	worker.postMessage(_CLIENT_INSTANCE_ID);
}

worker.onmessage = function(e){
	document.getElementById("decyphered_txt_sub").innerHTML = e.data;
}
</script>
    </head>
<body onload="w_start();">
<div id="maincontainer">
        <div style="max-width: 211px; font-family: 'Courier New', Courier, monospace" id="decyphered_txt_sub"></div>
</div>
<div id="likelihood_progress"></div>
</body>
</html>
