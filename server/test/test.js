function getRandomInt(max) {
    return Math.floor(Math.random() * max);
}

function getRandomString(nChar) {
    var str = "";
    for(i=0; i<nChar; i++)
        str += "abcdefghilmnopqrstuvz"[Math.floor(Math.random() * 21)];

    return str;
}


function prepareMock(nOfRecords){
    var data = [];

    for(j=0; j<nOfRecords; j++){
        var lh = getRandomInt(123456);
        var ck = getRandomString(5+getRandomInt(10));
        data[j] = {'likelihood':lh, 'cryptokey': ck};
    }


    return data;
}

