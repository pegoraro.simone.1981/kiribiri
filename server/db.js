db = Array();

function dbcount(){
	cnt = 0;

	db.forEach(element => {
		cnt += element.cryptokeys.length;
	});

	return cnt;
}

function dbinit(){
	//TODO: read from file
}

function insert(obj){
	entry = db.filter(function(elem){ return obj.likelihood == elem.likelihood;})[0]
	if(entry === undefined){
		cks = Array();
		cks[0] = obj.cryptokey;
		db[db.length] = {'likelihood':obj.likelihood, 'cryptokeys': cks};
	}
	else{
		if(undefined === entry.cryptokeys.filter(function(ck){ return ck == obj.cryptokey})[0])
			entry.cryptokeys[entry.cryptokeys.length] = obj.cryptokey;
	}
}

function readstochastic(nOfRecords){
	db.sort((a,b) => b.likelihood - a.likelihood);
	max_likelihood = db[0].likelihood;
	
	dbcnt = dbcount();
	resp_data = db.filter(elem => {return elem.likelihood > Math.random()*max_likelihood*(dbcnt/(2*nOfRecords));});
	var resp = resp_data.reduce((obj, item) => (obj[item.cryptokeys[0]] = item.likelihood, obj) ,{});
	return resp;
}

module.exports = {
	dbinit: dbinit,
	insert: insert,
	readstochastic: readstochastic,
	dbcount: dbcount
}
